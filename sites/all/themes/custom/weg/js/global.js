(function ($) {
    $(document).ready(function () {


        $('.front-news-items-slider, .front-pubs-items-slider, .front-projects-items-slider').bxSlider({

            minSlides: 1,
            maxSlides: 1,
            slideMargin: 0,
            controls: 1,
            pager: true,
            pagerType: 'short',
            pagerShortSeparator: '/'


        });


        /*
         CONTACT MAP
         */

        /**
         * Created by nacarqeq1a on 5/28/14.
         */
            // The latitude and longitude of your business / place
        (function ($) {
            $(document).ready(function (event) {
                var position = [41.691644, 44.814201];

                function showGoogleMaps() {

                    var latLng = new google.maps.LatLng(position[0], position[1]);
                    var center = new google.maps.LatLng(position[0], position[1]);
                    var styles = [
                        {
                            "featureType": "landscape",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {"color": "#f5f5f5"}
                            ]
                        }, {
                            "featureType": "poi",
                            "elementType": "geometry",
                            "stylers": [
                                {"color": "#eaeaea"}
                            ]
                        }, {
                            "featureType": "water",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {"color": "#c22679"}
                            ]
                        }, {
                            "featureType": "road.highway",
                            "elementType": "geometry",
                            "stylers": [
                                {"color": "#b4b4b4"}
                            ]
                        }, {
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {"color": "#535353"}
                            ]
                        }, {
                            "featureType": "transit",
                            "elementType": "labels.icon",
                            "stylers": [
                                {"hue": "#3c00ff"},
                                {"saturation": -48},
                                {"lightness": 40},
                                {"gamma": 0.95}
                            ]
                        }, {}
                    ]
                    var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});

                    var mapOptions = {

                        scrollwheel: false,

                        zoom: 15, // initialize zoom level - the max value is 21
                        streetViewControl: true, // hide the yellow Street View pegman
                        scaleControl: true, // allow users to zoom the Google Map
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        center: center,


                        scaleControl: false,
                        zoomControl: true,
                        mapTypeControl: false,
                        scrollwheel: false,


                        disableDoubleClickZoom: true,
                        mapTypeControlOptions: {
                            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
                        }
                    };


                    //console.log(latLng);
                    map = new google.maps.Map(document.getElementById('googlemaps'), mapOptions);

                    //map style
                    map.mapTypes.set('map_style', styledMap);
                    map.setMapTypeId('map_style');


                    // Show the default red marker at the location
                    marker = new google.maps.Marker({
                        position: latLng,
                        map: map,
                        draggable: false,
                        animation: google.maps.Animation.DROP,
                        icon: 'http://markup.omedialab.com/weg/images/map-selector.png'

                        // icon: '../sites/default/themes/weg/images/map-selector.png'

                    });

                }

                google.maps.event.addDomListener(window, 'load', showGoogleMaps);

            });

        }(jQuery));


        $('.front-leading-news-item:not(:first-child)').each(function () {


            var titleh = $(this).find(".front-leading-news-title").height();
            console.log(titleh)

            $(this).find(".front-leading-news-text-box-in").css("height", titleh)


        });


        $('.front-partners-slider').bxSlider({

            minSlides: 6,
            maxSlides: 6,
            slideMargin: 0,
            slideWidth: 167,
            controls: 1,
            pager: 0,

            nextSelector: '.custom-slider-next',
            prevSelector: '.custom-slider-prev',


        });


        $('.front-promo-slider').bxSlider({

            minSlides: 1,
            maxSlides: 1,
            slideMargin: 0,
            controls: 1,
            pager: 0,

            nextSelector: '.custom-slider-next',
            prevSelector: '.custom-slider-prev',


        });


        $('.havechild').each(function () {


            var submh = $(this).find(".submenu").height() + 13 + 6 + 40;


            //	var subpos = 100-$(this).width()/2 ;


            //	$(this).find(".submeniu-block").css("left", -subpos);


            $(this).mouseover(function () {


                $(this).find(".submeniu-block").css("height", submh);


                $(this).find(".submeniu-block").css("padding-top", 17);


            });

            $(this).mouseleave(function () {
                $(this).find(".submeniu-block").css("height", 0);


                $(this).find(".submeniu-block").css("padding-top", 0);


            });


            $('.havechild2').each(function () {


                var submh2 = $(this).find(".submenu2").height();


                $(this).mouseover(function () {


                    $(this).find(".submeniu-block2").css("height", submh2);


                });

                $(this).mouseleave(function () {
                    $(this).find(".submeniu-block2").css("height", 0);


                });


            });

        });


        $('.check-style').iCheck({
            checkboxClass: 'icheckbox_minimal-purple',
            radioClass: 'iradio_minimal-purple',
            increaseArea: '20%' // optional
        });


    });


}(jQuery));


