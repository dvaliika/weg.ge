<?php print $menu_footer; ?>
<footer class="footer">
    <div class="container">
        <div class="row clearfix">
            <div class="col-md-8">
                <nav class="footer-menu">
                    <h3><?php print t('About us'); ?></h3>
                    <?php print $menu_footer_about_us; ?>
                </nav>
                <nav class="footer-menu">
                    <h3><?php print t('Projects'); ?></h3>
                    <?php print $menu_footer_projects; ?>
                </nav>
                <nav class="footer-menu">
                    <h3><?php print t('Publications'); ?></h3>
                    <?php print $menu_footer_publications; ?>
                </nav>
                <nav class="footer-menu">
                    <h3><?php print t('Contact'); ?></h3>
                    <?php print $menu_footer_contact; ?>
                </nav>
            </div>
            <div class="col-md-4 right footer-right">
                <div class="footer-socs">
                    <a href="#">
                        <?php print svg_icon('facebook') ?>
                    </a>
                    <a href="#">
                        <?php print svg_icon('twitter') ?>
                    </a>
                    <a href="#">
                        <?php print svg_icon('linkedin') ?>
                    </a>
                    <a href="#">
                        <?php print svg_icon('youtube') ?>
                    </a>
                </div>
                <div class="copyright"><span> © 2013-2016 LTD. WEG All rights reserved.</span> </div>
                <div class="subscribe-module">
                    <div class="subscribe-desc">
                        <?php print t('Enter your e-mail and check categories for subscire') . ':'; ?>
                    </div>
                    <div class="subscribe-form">
                        <div class="subscribe-check">
                            <div class="subscribe-check-item">
                                <input type="checkbox" id="subscribe-check-newslwtter" class="check-style" />
                                <label for="subscribe-check-newslwtter"><?php print t('Newsletter'); ?></label>
                            </div>
                            <div class="subscribe-check-item">
                                <input type="checkbox" id="subscribe-check-pubs" class="check-style"/>
                                <label for="subscribe-check-pubs"><?php print t('Publications'); ?></label>
                            </div>
                            <div class="subscribe-check-item" >
                                <input type="checkbox" id="subscribe-check-news" class="check-style"/>
                                <label for="subscribe-check-news"><?php print t('News'); ?></label>
                            </div>
                            <div class="subscribe-check-item">
                                <input type="checkbox" id="subscribe-check-event" class="check-style" />
                                <label for="subscribe-check-event"><?php print t('Event'); ?></label>
                            </div>
                        </div>
                        <div class="subscribe-send-form clearfix">
                            <div class="right">
                                <input type="text" placeholder="your email" class="subscribe-input" />
                                <input type="button" value="Go" class="subscribe-button" />
                                <div class="subscribe-text">
                                    <?php print t('We never spam'); ?>!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="created" >
            Website by <a href="#">Omedia</a>
        </div>
    </div>
</footer>
