<?php if(!empty($menu)): ?>
    <ul>
    <?php foreach($menu as $item) :?>
        <li>
            <a href="<?php print $item['url'];?>"><?php print $item['title']; ?></a>
        </li>
    <?php endforeach; ?>
    </ul>
<?php endif; ?>