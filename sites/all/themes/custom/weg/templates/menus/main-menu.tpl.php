<?php if(!empty($menu)): ?>
    <?php if($level == 1) :?>
        <span class="submeniu-block">
    <?php endif; ?>
    <ul class="<?php if($level == 1) print 'submenu'; ?>">
        <?php foreach ($menu as $key => $item):?>
            <?php
            $active = ($item['is_active']) ? 'active' : '';
            $havechild = (!empty($item['below'])) ? 'havechild' : '';
            ?>
            <li class="<?php print $class . ' ' . $havechild . ' ' . $active?>">
                <a href="<?php print $item['url'] ?>"><?php print $item['title']; ?></a>
                <?php
                if (!empty($item['below'])) {
                    print $item['below'];
                }
                ?>
            </li>
        <?php endforeach; ?>
    </ul>
    <?php if($level == 1) :?>
        </span>
    <?php endif; ?>
<?php endif; ?>
