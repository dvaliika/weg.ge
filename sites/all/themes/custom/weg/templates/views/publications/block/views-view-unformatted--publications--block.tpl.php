<?php
    $count = count($rows);
?>
<section class="front-pubs-block  section-block">
    <h3 class="block-title"><?php print t('Publications'); ?></h3>

    <div class="front-pubs-items-slider">
        <?php foreach ($rows as $key => $row) : ?>
            <?php if($key % 3 == 0) : ?>
                <div class="front-pubs-items">
                    <div class="front-pubs-row clearfix row">
            <?php endif; ?>

            <?php print $row; ?>

            <?php if($key % 3 == 2 || $key == $count - 1) : ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</section>