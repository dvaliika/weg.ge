<?php
$count = count($rows);
?>
<div class="front-content-left col-md-9">
    <section class="front-pubs-block  section-block">
        <h3 class="page-title"><?php print t('Publications');?></h3>
        <div class="front-pubs-items-slider">
            <?php foreach ($rows as $key => $row) : ?>
                <?php if($key % 12 == 0) : ?>
                    <div class="front-pubs-items">
                <?php endif; ?>

                <?php if($key % 3 == 0) : ?>
                    <div class="front-pubs-row clearfix row">
                <?php endif; ?>

                <?php print $row; ?>

                <?php if($key % 3 == 2 || $key == $count - 1) : ?>
                    </div>
                <?php endif; ?>

                <?php if($key % 12 == 11 || $key == $count - 1) : ?>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </section>
</div>