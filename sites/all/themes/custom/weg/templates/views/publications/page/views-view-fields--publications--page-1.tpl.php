<article class="front-pubs-item col-md-4" >
    <a href="<?php print $fields['field_files']->content; ?>" target="_blank">
        <span class="pubs-ic">
            <?php print svg_icon('pdf') ?>
        </span>
        <span class="front-pubs-text">
            <span class="front-pubs-date">
                <?php print $fields['created']->content; ?>
            </span>
            <h2 class="front-pubs-title">
                <?php print $fields['title']->content; ?>
            </h2>
            <p class="front-pubs-desc">
                <?php print $fields['body']->content; ?>
            </p>
        </span>
    </a>
</article>