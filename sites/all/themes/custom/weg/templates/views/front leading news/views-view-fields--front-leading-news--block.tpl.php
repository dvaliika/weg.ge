<article class="front-leading-news-item">
    <a href="<?php print $fields['field_url']->content; ?>">
        <img src="<?php print $fields['field_image']->content; ?>" alt=""/>
        <span class="front-leading-news-text">
            <span class="front-leading-news-text-box">
                <span class="front-leading-news-text-box-in">
                    <span class="front-leading-news-date">
                        <?php print $fields['created']->content; ?>
                    </span>
                    <h2 class="front-leading-news-title">
                        <?php print $fields['title']->content; ?>
                    </h2>
                    <p class="front-leading-news-desc">
                        <?php print $fields['body']->content; ?>
                    </p>
                    <?php if($fields['counter']->content > 0) :?>
                        <span class="readmore"><?php print ('Read more');?> > </span>
                    <?php endif; ?>
                </span>
            </span>
        </span>
    </a>
</article>