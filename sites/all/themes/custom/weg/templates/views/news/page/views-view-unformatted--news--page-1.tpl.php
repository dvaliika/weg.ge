<?php
$count = count($rows);
?>
<div class="front-content-left col-md-9">
    <section class="front-news-block section-block">
        <h3 class="page-title"><?php print t('NEWS');?></h3>
        <div class="front-news-items-slider">
            <?php foreach ($rows as $key => $row) : ?>
                <?php if($key % 10 == 0) : ?>
                    <div class="front-news-items">
                <?php endif; ?>

                <?php if($key % 2 == 0) : ?>
                    <div class="front-news-row clearfix row">
                <?php endif; ?>

                <?php print $row; ?>

                <?php if($key % 2 == 1 || $key == $count - 1) : ?>
                        </div>
                <?php endif; ?>

                <?php if($key % 10 == 9 || $key == $count - 1) : ?>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
         </div>
    </section>
</div>