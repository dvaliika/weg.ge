<article class="front-news-item col-md-6">
    <a href="<?php print $fields['path']->content; ?>">
        <span class="front-news-item-img">
            <img src="<?php print $fields['field_image']->content; ?>" alt=""/>
        </span>
        <span class="front-news-text">
            <span class="front-news-text-box">
                <span class="front-news-date">
                    <?php print $fields['created']->content; ?>
                </span>
                <h2 class="front-news-title">
                    <?php print $fields['title']->content; ?>
                </h2>
                <p class="front-news-desc">
                    <?php print $fields['body']->content; ?>
                </p>
            </span>
        </span>
    </a>
</article>