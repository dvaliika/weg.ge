<div class="front-content-left col-md-9">
    <section class="front-projects-block section-block">
        <h3 class="page-title"><?php print t('Projects'); ?></h3>
        <div class="front-projects-items-slider">
            <?php foreach ($rows as $key => $row) : ?>
                <?php if($key % 5 == 0) : ?>
                    <div class="front-projects-items">
                <?php endif; ?>

                <?php print $row; ?>

                <?php if($key % 5 == 4 || $key == $count - 1) : ?>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </section>
</div>