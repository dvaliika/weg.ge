<article class="promo-news-item">
    <a href="<?php print $fields['path']->content; ?>">
        <img src="<?php print $fields['field_image']->content; ?>" alt=""/>
        <h2 class="promo-news-title"><?php print $fields['title']->content; ?></h2>
    </a>
</article>