<article class="front-projects-item clearfix" >
    <a href="<?php print $fields['path']->content; ?>">
        <span class="front-projects-item-img">
            <img src="<?php print $fields['field_image']->content; ?>" alt="" />
        </span>
        <span class="front-projects-text">
            <span class="front-projects-text-box">
                <h2 class="front-projects-title">
                    <?php print $fields['title']->content; ?>
                </h2>
                <p class="front-projects-desc">
                    <?php print $fields['body']->content; ?>
                </p>
            </span>
        </span>
        <span class="projects-readmore">
            <?php print t('Learn More'); ?>
            <?php print svg_icon('arrow-right2') ?>
        </span>
    </a>
</article>