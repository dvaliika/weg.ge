<section class="front-promo-items">

    <div class="front-promo-slider">
        <?php foreach($rows as $row) :?>
            <?php print $row; ?>
        <?php endforeach; ?>
    </div>

    <div class="bx-custom-arrows clearfix">
        <div class="custom-slider-next">
            <?php print svg_icon('arrow-right') ?>
        </div>
        <div class="custom-slider-prev">
            <?php print svg_icon('arrow-left') ?>
        </div>
    </div>
</section>