<?php global $language; ?>

<div class="lang-selector">
    <?php foreach ($links as $key => $link): ?>
        <a class="<?php if($link['language']->language == $language->language) print 'active'?>"
           href="<?php print url($link['href'], array('language' => $link['language'])) ?>">
            <?php print strtoupper(substr($link['language']->name, 0, 3)); ?>
        </a>
        <?php if($key == 'en') :?>
            <span class="separator"> /</span>
        <?php endif; ?>
    <?php endforeach; ?>
</div>
