<header class="header">
    <div class="container">
        <div class="row clearfix">
            <div class="logo col-md-3">
                <a href="<?php print $front_page; ?>">
                    <img src="<?php print $logo; ?>" alt=""/>
                </a>
            </div>
            <div class="col-md-9 header-right clearfix">
                <div class="right">
                    <div class="header-top">
                        <?php print $lang_switcher; ?>
                        <div class="header-socs">
                            <a href="#">
                                <?php print svg_icon('facebook') ?>
                            </a>
                            <a href="#">
                                <?php print svg_icon('twitter') ?>
                            </a>
                            <a href="#">
                                <?php print svg_icon('linkedin') ?>
                            </a>
                            <a href="#">
                                <?php print svg_icon('youtube') ?>
                            </a>
                        </div>
                        <div class="search clearfix">
                            <input type="text" placeholder="type here for search" class="search-inp"/>
                            <input type="submit" class="search-but"/>
                        </div>
                    </div>
                    <div class="header-bottom  clearfix">
                        <nav class="main-menu">
                            <?php print $menu_main; ?>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>