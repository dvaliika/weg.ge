<?php require_once('header.tpl.php'); ?>

    <div class="container clearfix main-content-container">
        <div class="row">
            <?php require_once('content.tpl.php'); ?>
            <?php print render($page['right_sidebar']); ?>
        </div>
        <?php print render($page['content_bottom']); ?>
    </div>

<?php require_once('footer.tpl.php'); ?>