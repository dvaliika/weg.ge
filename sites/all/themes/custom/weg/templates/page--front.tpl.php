<?php require_once('header.tpl.php'); ?>

    <div class="page-content">
        <div class="container">
            <div class="front-top-news clearfix">
                <?php print render($page['content_top']); ?>

                <section class="front-tweeter-timeline">
                    <a class="twitter-timeline"
                       data-widget-id="600720083413962752"
                       href="https://twitter.com/fabric"
                       data-screen-name="fabric">
                        Tweets by @fabric
                    </a>
                </section>
            </div>

            <div class="front-content clearfix">
                <div class="row">
                    <div class="front-content-left col-md-9">
                        <?php require_once('content.tpl.php'); ?>
                    </div>
                    <?php print render($page['right_sidebar']); ?>
                </div>
            </div>

            <?php print render($page['content_bottom']); ?>
        </div>
    </div>

<?php require_once('footer.tpl.php'); ?>