<?php
$body  = field_get_items('node', $node, 'body');
$files = field_get_items('node', $node, 'field_files');
?>
<div class="front-content-left col-md-9">
    <section class="basic-block section-block">
        <h3 class="page-title"><?php print $node->title; ?></h3>
        <div class="basic-text">
            <?php print $body[0]['value']; ?>
        </div>
        <?php if(!empty($files)) :?>
            <div class="article-meta">
                <span class="article-meta-lab">
                    <?php print svg_icon('attached') ?>
                    <?php print t('Attached Files'); ?>:
                </span>
                <span>
                    <?php foreach($files as $file) :?>
                        <a href="<?php print file_create_url($file['uri']); ?>" target="_blank">
                            <?php print $file['description']; ?>
                        </a>
                    <?php endforeach; ?>
                </span>
            </div>
        <?php endif; ?>
    </section>
</div>