<?php
function weg_form_system_theme_settings_alter(&$form, &$form_state) {
  $form['theme_settings']['render_submenus'] = array(
    '#type' => 'checkbox',
    '#title' => t('Render submenus in menu'),
    '#default_value' => theme_get_setting('render_submenus'),
  );
}