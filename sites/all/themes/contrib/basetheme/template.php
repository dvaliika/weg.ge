<?php

/*
 * implements hook_preprocess_html()
 */
function basetheme_preprocess_html(&$vars) {
  global $base_url, $language;

  /* remove fuckin nodes (promoted to front page) from homepage */
  if ($vars['is_front']) {
    unset($vars['page']['content']['system_main']['nodes']);
    unset($vars['page']['content']['system_main']['pager']);
  }

  /* theme_path */
  $vars['theme_path'] = $base_url . '/' . path_to_theme() . '/';
	drupal_add_js(array('theme_path' => $vars['theme_path']), 'setting');
	
  /* base url */
  $vars['base_url'] = $base_url;

  // /* language */
  // drupal_add_js(array('language' => $language), 'setting');
}



/*
 * implements hook_css_alter()
 */
function basetheme_css_alter(&$css) {
	unset($css[drupal_get_path('module','system').'/system.base.css']);
	unset($css[drupal_get_path('module','system').'/system.theme.css']);
	unset($css[drupal_get_path('module','system').'/system.menus.css']);
	//unset($css[drupal_get_path('module','system').'/system.messages.css']);
	unset($css[drupal_get_path('module','calendar').'/css/calendar_multiday.css']);
	unset($css[drupal_get_path('module','date').'/date_views/css/date_views.css']);
	unset($css[drupal_get_path('module','comment').'/comment.css']);
	unset($css[drupal_get_path('module','date_api').'/date.css']);
	unset($css[drupal_get_path('module','date_popup').'/themes/datepicker.1.7.css']);
	unset($css[drupal_get_path('module','field').'/theme/field.css']);
	unset($css[drupal_get_path('module','node').'/node.css']);
	unset($css[drupal_get_path('module','search').'/search.css']);
	unset($css[drupal_get_path('module','user').'/user.css']);
	unset($css[drupal_get_path('module','views').'/css/views.css']);
	unset($css[drupal_get_path('module','ctools').'/css/ctools.css']);
}

/*
 * implements hook_js_alter()
 */
function basetheme_js_alter(&$js) {
}


/*
 * implements hook_preprocess_page()
 */
function basetheme_preprocess_page(&$vars) {
  global $base_url;

  /* remove fuckin nodes (promoted to front page) from homepage */
  if ($vars['is_front']) {
    unset($vars['page']['content']['system_main']['nodes']);
    unset($vars['page']['content']['system_main']['pager']);
  }
	
	
	/* add js */
  // drupal_add_js(drupal_get_path('theme', 'basetheme') .'/js/html5shiv.js');
  // drupal_add_js(drupal_get_path('theme', 'basetheme') .'/js/modernizr.js');
  // drupal_add_js(drupal_get_path('theme', 'basetheme') .'/js/jquery.waitforimages.min.js');
  // drupal_add_js(drupal_get_path('theme', 'basetheme') .'/js/jquery.scrollTo.min.js');
  // drupal_add_js(drupal_get_path('theme', 'basetheme') .'/js/global.js');
	
	
  /* theme_path */
  $vars['theme_path'] = $base_url . '/' . path_to_theme() . '/';
	drupal_add_js(array('theme_path' => $vars['theme_path']), 'setting');
	
  /* base url */
  $vars['base_url'] = $base_url;

  /* main menu */
  $vars['menu_main'] = get_menu('main-menu', 0, 'main-menu');

  /* footer menu */
  $vars['menu_footer'] = get_menu('menu-footer', '', 0, 0);

  // /* language switcher */
  // $vars['lang_switcher'] = get_language_switcher();

  /* user-login */
  if(!user_is_logged_in() && current_path() == 'user'){
            drupal_goto('user/login');
  }
  if(current_path() == 'user/login'){
	drupal_add_css(path_to_theme().'/css/omedia_login/user-login.css');
	drupal_add_js(path_to_theme().'/js/omedia_login/user-login.js');
  }
}


/*
 * main menu generation
 * 
 * $menu: drupal's menu name OR array containing menu objects
 * $level: which level (in case of submenus) to render (for recursion)
 * $render_submenus: explicitly define whether render or not submenus
 */
function get_menu($menu, $level=0, $tplfile = '', $render_submenus = NULL) {
  global $language;
  
  if (!is_array($menu)) {
    $menu = menu_tree_all_data($menu);
  }
  
  $menu_for_theme = array();
	
  foreach($menu as $key => $item) {

    if (($item['link']['language'] != $language->language) && $item['link']['language'] != 'und') {
      continue;
    }
        
    $below = NULL;
    if ($render_submenus === NULL) {
      $render_submenus = theme_get_setting('render_submenus');
    }
    if(!empty($item['below']) && $render_submenus) {
      $below = get_menu($item['below'], $level+1, 'main-menu');
    }

    $item = $item['link'];
    $title = $item['link_title'];
    if ($item['link_path'] == '') {
      $item['link_path'] = '<front>';
    }

    if(!empty($item['options']['fragment']))
      $url = url($item['link_path'], array('fragment' => $item['options']['fragment'] ));
    else
      $url = url($item['link_path']);
    
    $active_trail = menu_get_active_trail();
    $is_active = ($item['in_active_trail'] == 1)
        || (current_path() == $url)
        || (($item['link_path'] == $active_trail[0]['href']) && (current_path() == 'node'))
        || (strstr(drupal_get_path_alias(current_path()), drupal_get_path_alias($item['link_path'])));


    $class = 'level_'.$level;
    if ($level == 0) {
      $class = 'root';
    }
		
    $menu_for_theme[] = array('url'       => $url,
                              'title'     => $title,
                              'is_active' => $is_active,
                              'below'     => $below);
  }
	
  return theme('get_menu', array('menu'    => $menu_for_theme,
                                 'class'   => $class,
                                 'tplfile' => $tplfile));
}




/*
 * drupal language links
 */
function get_language_links() {
  $path = drupal_is_front_page() ? '<front>' : $_GET['q'];
  $languages = language_list('enabled');
  $links = array();
  foreach ($languages[1] as $language) {
    $links[$language->language] = array(
      'href' => $path, 
      'title' => $language->native, 
      'language' => $language, 
      'attributes' => array('class' => 'language-link'),
    );
  }
  drupal_alter('translation_link', $links, $path);
  return $links;
}




/*
 * implements hook_theme()
 */
function basetheme_theme($existing, $type, $theme, $path) {
  return array(
    'get_menu' => array(
      'variables' => array(
        'menu' => array(),
        'class' =>  NULL,
        'tplfile' =>  NULL,
      ),
      'template' => 'templates/menus/menu'
    ),
		
    'get_language_links' => array(
      'variables' => array(
        'links' => array(),
      ),
      'template' => 'templates/language_switcher'
    ),
  );
}



/*
 * implements get_language_switche
 */
function get_language_switcher() {
	global $language;
  if (drupal_multilingual()) {
    $path = drupal_is_front_page() ? '<front>' : $_GET['q'];
    $links = language_negotiation_get_switch_links('language', $path);
		unset($links->links[$language->language]);
    if (isset($links->links)) {
			$output = theme('get_language_links', array('links' => $links->links));
      return $output;
    }
  }
	return;
}

