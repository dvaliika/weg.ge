<?php global $base_url; ?>
<div id="user-login-form-wrapper" class="user-login-wrapper">
    <img class="omedia-logo" src="<?php print $base_url.'/'.path_to_theme().'/images/omedia_login/omedialogo.png' ?>">
    <?php
    $elements = drupal_get_form("user_login");
    $form = drupal_render($elements);
    echo $form;
    ?>
    <img class="drupal-logo" src="<?php print $base_url.'/'.path_to_theme().'/images/omedia_login/drupal_logo.png' ?>">
</div>