<?php if(!empty($menu)): ?>
  <ul class="<?php print $class; ?>">
  <?php foreach ($menu as $key => $item):?>
    <li <?php if ($item['is_active']) print 'class="active"'; ?>>
      <a href="<?php print $item['url'] ?>"><?php print $item['title']; ?></a>
      <?php if (!empty($item['below'])) { print $item['below']; } ?>
    </li>
  <?php endforeach; ?>
  </ul>
<?php endif; ?>