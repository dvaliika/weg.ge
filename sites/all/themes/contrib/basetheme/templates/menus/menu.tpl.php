<?php if(!empty($menu)): ?>
  <?php
    if(file_exists(realpath(__DIR__).'/'.$tplfile.'.tpl.php')) {
      require(realpath(__DIR__).'/'.$tplfile.'.tpl.php');
    } else {
      require(realpath(__DIR__).'/default-menu.tpl.php');
    }
  ?>
<?php endif; ?>