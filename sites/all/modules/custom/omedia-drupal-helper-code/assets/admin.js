
(function ($, Drupal, window, document, undefined) {


$(document).ready(function(){

	//
	// article images auto height
	//
	var $bodyField = $(".node-form.node-article-form #edit-body");
	var $picsField = $(".node-form.node-article-form #edit-field-multiimage");
	//
	if( $bodyField.length > 0 && $picsField.length > 0 ) {
		var targHeight = parseInt($bodyField.height());
		if( $bodyField.find(".filter-wrapper").length > 0 ) {
			targHeight += parseInt($bodyField.find(".filter-wrapper").height());
		}
		$picsField.css({
			height : targHeight,
			overflowY : 'scroll'
		});
	}
	
}); // end jquery document ready

})(jQuery, Drupal, this, this.document);