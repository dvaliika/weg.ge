<?php
/**
 * @file
 * omedia_configuration.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function omedia_configuration_filter_default_formats() {
  $formats = array();

  // Exported format: Filtered HTML.
  $formats['filtered_html'] = array(
    'format' => 'filtered_html',
    'name' => 'Filtered HTML',
    'cache' => 1,
    'status' => 1,
    'weight' => 0,
    'filters' => array(
      'float_filter' => array(
        'weight' => -49,
        'status' => 1,
        'settings' => array(
          'img_tag' => 'span',
          'table_tag' => 'span',
          'figure_tag' => 'div',
        ),
      ),
      'filter_htmlcorrector' => array(
        'weight' => -48,
        'status' => 1,
        'settings' => array(),
      ),
      'htmlpurifier_basic' => array(
        'weight' => -47,
        'status' => 1,
        'settings' => array(
          'htmlpurifier_help' => 1,
          'htmlpurifier_basic_config' => array(
            'Attr.EnableID' => 0,
            'AutoFormat.AutoParagraph' => 1,
            'AutoFormat.Linkify' => 0,
            'AutoFormat.RemoveEmpty' => 1,
            'HTML.Allowed' => '*[class|style]
a[href|target|rel]
em
strong
blockquote
ul
ol
li
img[width|height|alt|title|src]
span
p
br
h1
h2
h3
h4
h5
table
tr
th
td
thead
tbody',
            'HTML.ForbiddenAttributes' => '',
            'HTML.ForbiddenElements' => '',
            'HTML.SafeObject' => 0,
            'Output.FlashCompat' => 0,
            'URI.DisableExternalResources' => 1,
            'URI.DisableResources' => 0,
            'Null_URI.Munge' => 1,
          ),
        ),
      ),
      'lightbox2_filter' => array(
        'weight' => -44,
        'status' => 1,
        'settings' => array(),
      ),
      'video_filter' => array(
        'weight' => -43,
        'status' => 1,
        'settings' => array(
          'video_filter_width' => 400,
          'video_filter_height' => 400,
          'video_filter_autoplay' => 0,
          'video_filter_related' => 0,
          'video_filter_html5' => 1,
        ),
      ),
    ),
  );

  // Exported format: Plain text.
  $formats['plain_text'] = array(
    'format' => 'plain_text',
    'name' => 'Plain text',
    'cache' => 1,
    'status' => 1,
    'weight' => 10,
    'filters' => array(
      'filter_html_escape' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_url' => array(
        'weight' => 1,
        'status' => 1,
        'settings' => array(
          'filter_url_length' => 72,
        ),
      ),
      'filter_autop' => array(
        'weight' => 2,
        'status' => 1,
        'settings' => array(),
      ),
    ),
  );

  return $formats;
}
