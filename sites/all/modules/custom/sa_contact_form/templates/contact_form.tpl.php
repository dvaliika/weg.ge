<div class="front-content-left col-md-9">
    <section class="contact-block section-block">
        <h3 class="page-title"><?php print t('Contact'); ?></h3>
        <div class="contact-form">
            <div class="row clearfix">
                <div class="col-md-4">
                    <div class="contact-info-department">
                        <div class="contact-title"><?php print t('World Experience for Georgia(WEG)'); ?></div>
                        <div class="contact-info-items">
                            <p class="contact-info-item contact-address"><span
                                    class="contact-label"><?php print t('Address') ;?>: </span><?php print $info['address']; ?></p>

                            <p class="contact-info-item contact-phone"><span
                                    class="contact-label"><?php print t('Telephone') ;?>: </span><?php print $info['phone']; ?></p>

                            <p class="contact-info-item contact-fax">
                                <span class="contact-label"><?php print t('Fax') ;?>: </span>
                                <?php print $info['fax']; ?>
                            </p>

                            <p class="contact-info-item contact-mail">
                                <span class="contact-label"><?php print t('E-mail') ;?>: </span>
                                <a href="mailto: <?php print $mail; ?>"><?php print $info['mail']; ?></a></p>
                        </div>
                    </div>
                    <div class="contact-info-department">
                        <div class="contact-title"><?php print t('Communication Officer') ;?></div>
                        <div class="contact-info-items">
                            <p class="contact-info-item contact-mail">E-mail: <?php print $info['com_officer']; ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 contact-right">
                    <div class="contact-title">
                        <?php print t('Send us a Message'); ?>
                    </div>

                    <?php print render($form); ?>
                </div>
            </div>
        </div>
        <div class="contact-map">
            <div id="googlemaps"></div>
        </div>
    </section>
</div>