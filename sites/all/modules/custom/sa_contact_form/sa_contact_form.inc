<?php

function sa_contact_page_form($form, &$form_state) {

    $svg = svg_icon('arrow-right2');

    $form['full_name'] = array(
        '#type' => 'textfield',
        '#title' => t('Name'),
        '#theme_wrappers' => array(),
        '#required' => TRUE,
        '#attributes' => array(
            'class' => array('contact-input'),
        ),
        '#prefix' => '<div class="contact-form-item">',
        '#suffix' => t('<label>@Name</label></div>',array('@Name' => 'Name'))
    );
    $form['email']    = array(
        '#type' => 'textfield',
        '#title' => t('e-Mail'),
        '#theme_wrappers' => array(),
        '#required' => TRUE,
        '#attributes' => array(
            'class' => array('contact-input'),
        ),
        '#prefix' => '<div class="contact-form-item">',
        '#suffix' => t('<label>@mail</label></div>',array('@mail' => 'Email'))
    );

    $form['body'] = array(
        '#type' => 'textarea',
        '#theme_wrappers' => array(),
        '#required' => TRUE,
        '#attributes' => array(
            'class' => array('contact-textarea'),
            'placeholder' => 'Your message'
        ),
        '#prefix' => '<div class="contact-form-item">',
        '#suffix' => '</div>'
    );

    $form['submit'] = array(
        '#markup' => "<button type='submit' class='contact-button'>" . t('Send') ." $svg</button>",
        '#prefix' => "<div class='contact-form-item-button'>",
        '#suffix' => "</div>",
    );

//    $form['security'] = array(
//        '#type'   => 'markup',
//        '#prefix' => '<div class="contact-form-item">',
//        '#suffix' => '</div>',
//        '#markup' => '<div class="g-recaptcha" data-sitekey="6LfNTh0TAAAAANugx-O0bYbYGq3SwIdjSmmeNmpF"></div>'
//    );

    return $form;
}

function sa_contact_page_form_validate($form, &$form_state) {
    $name  = $form_state['values']['full_name'];
    $email = $form_state['values']['email'];
    $body = $form_state['values']['body'];

    if(!valid_email_address($email)) {
        form_set_error('email', t('not correct mail'));
    }

    if (empty($name)) {
        form_set_error('full_name', t('name is empty'));
    }

    if (empty($body)) {
        form_set_error('body', t('body text is empty'));
    }
}

function sa_contact_page_form_submit($form, &$form_state) {
    $mail_to = variable_get('sa_contact_mail', '');

    $name = $form_state['values']['full_name'];
    $from = $form_state['values']['email'];
    $body = $form_state['values']['body'];
    $text = $name . " ". $body . " " . $from;

    $params = array(
        'body'      => $text,
        'subject'   => t('Contact'),
        'headers'   => 'simple',
    );

    drupal_mail('contact_form', 'send_link', $mail_to, language_default(), $params, 'demo@demo.com', TRUE);
}